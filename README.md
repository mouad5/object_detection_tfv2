Easy installation of [Tensorflow Object Detection API](https://github.com/tensorflow/models/tree/master/research/object_detection) for tensorflow v2 with backwards compatiblity with tensorflow.compat.v1
# Installation

* Download corresponding protoc binary from https://github.com/protocolbuffers/protobuf/releases/

* `PROTOC` Environment variable should be set so that `setup.py` can compile as
  part of the `setup.py` execution.

## with setup.py
```bash
# Download and setup all required dependencies required for tensorflow object 
# detection libarary to work correctly.
$ python setup.py install
```

## or with pip
* with pip for tensorflow: 
```
pip install git+https://gitlab.com/mouad5/object_detection_tfv2.git#egg=object_detection[tf]

```

* Or for tensorflow with GPU support,

```
pip install git+https://gitlab.com/mouad5/object_detection_tfv2.git#egg=object_detection[tf-gpu]

```
# Usage

```
import object_detection
```


