"""Setup script for object_detection with TF2.0."""
import os
from setuptools import find_packages
from setuptools import setup
import sys
import os
from distutils.spawn import find_executable
import subprocess
from typing import List

file_dir = os.path.dirname(os.path.realpath(__file__))


def runcmd(cmds: List[str]):
    return subprocess.call(cmds, shell=True)


# Find the Protocol Compiler.
if 'PROTOC' in os.environ and os.path.exists(os.environ['PROTOC']):
  protoc = os.environ['PROTOC']
else:
  protoc = find_executable("protoc")

if not protoc:
    error = "protoc command not found in PATH."\
    " install the protoc command system wide or set the PROTOC env"\
    " variable with protoc executable path."
    print(error)
    sys.exit(1)

proto_commands = [
    #"cd ./tf_object_detection/research;" +
    protoc +
    " object_detection/protos/*.proto" +
    " --python_out=."
]
# Pull the upstream object_detection code and package it.
runcmd(['git', 'submodule', 'update', '--init'])
runcmd(proto_commands)


with open(os.path.join(file_dir, 'README.md')) as f:
    long_description = f.read()

# Note: adding apache-beam to required packages causes conflict with
# tf-models-offical requirements. These packages request for incompatible
# oauth2client package.
REQUIRED_PACKAGES = [
    # Required for apache-beam with PY3
    'avro-python3<=1.9.2.1',
    'apache-beam<=2.33.0',
    'Pillow<=8.4.0',
    'lxml<=4.6.4',
    'matplotlib<=3.4.3',
    'Cython<=0.29.24',
    'contextlib2<=21.6.0',
    'tf-slim<=1.1.0',
    'six<=1.15.0',
    'pycocotools<=2.0.2',
    'lvis<=0.5.3',
    'scipy<=1.7.2',
    'pandas<=1.3.4',
    'tf-models-official<=2.5.1',
    'tensorflow-io<=0.21.0'
]

extras_require = {
    'tf': ['tensorflow>=2.0'],
    'tf-gpu': ['tensorflow-gpu>=2.0'],
}

setup(
    name='object_detection',
    version='0.1',
    install_requires=REQUIRED_PACKAGES,
    extras_require=extras_require,
    include_package_data=True,
    packages=(
        [p for p in find_packages() if p.startswith('object_detection')] +
        find_packages(where=os.path.join('.', 'slim'))),
    package_dir={
        'datasets': os.path.join('slim', 'datasets'),
        'nets': os.path.join('slim', 'nets'),
        'preprocessing': os.path.join('slim', 'preprocessing'),
        'deployment': os.path.join('slim', 'deployment'),
        'scripts': os.path.join('slim', 'scripts'),
    },
    description='Tensorflow Object Detection Library',
    python_requires='>3.6',
)
